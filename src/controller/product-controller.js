module.exports = (service) => ({
  insertProduct: async (req, res) =>
    await service
      .addNewProduct(req.params.name)
      .then((data) => res.json(data))
      .catch((error) => {
        console.log(error);
        return res.status(500).send("internal server error");
      }),
});
