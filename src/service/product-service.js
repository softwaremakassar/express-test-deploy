module.exports = (model) => ({
  addNewProduct: async (name) => {
    try {
      const dataProduct = await model.create({ name });
      return dataProduct;
    } catch (error) {
      throw error;
    }
  },
});
