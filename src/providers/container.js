// Init database
const db = require("../../models");
const products = require("../../schema/products");
// Init All Service
const customerService = require("../service/customer-service")(db);
const productService = require("../service/product-service")(products);
// Init All Controller
const customerController = require("../controller/customer-controller")(
  customerService
);
const productController = require("../controller/product-controller")(
  productService
);
// Intit All Router
const customerRouter = require("../router/customer-router")(customerController);
const productRouter = require("../router/product-router")(productController);

module.exports = {
  customerRouter,
  productRouter,
};
